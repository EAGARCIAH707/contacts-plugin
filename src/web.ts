import { WebPlugin } from '@capacitor/core';
import { ContactPluginPlugin } from './definitions';

export class ContactPluginWeb extends WebPlugin implements ContactPluginPlugin {
  constructor() {
    super({
      name: 'ContactPlugin',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }
  async getContacts(): Promise<{ result: any[] }> {
    return {
      result: [{
        firsName: 'Name Test',
        lastName: 'LastName test',
        phone: '123456789',
      }],
    };
  }
}

const ContactPlugin = new ContactPluginWeb();

export { ContactPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(ContactPlugin);
